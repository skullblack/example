export const ADD_LIST = 'ADD_LIST';
export const EDIT_LIST = 'EDIT_LIST';
export const CHECK_ALL_LIST = 'CHECK_ALL_LIST';
export const EDIT_CHECK_LIST = 'EDIT_CHECK_LIST';
export const DELETE_LIST = 'DELETE_LIST';
export const DELETE_ALL_LIST = 'DELETE_ALL_LIST';

//Action Creator
export const addList = () => {
  return {
    type: ADD_LIST,
    payload: 'data',
  };
};

export const deleteList = () => {
  return {
    type: DELETE_LIST,
    payload: 'data',
  };
};
