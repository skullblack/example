import {ADD_LIST, DELETE_LIST, EDIT_LIST, EDIT_CHECK_LIST, CHECK_ALL_LIST, DELETE_ALL_LIST} from '../action/listaction';
import {v4 as uuidv4} from 'uuid';

const initialState = {
  list: [
    {
      id: uuidv4(),
      avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png',
      title: 'Ant Design Title 1',
      detail: 'Ant Design, a design language for background applications, is refined by Ant UED Team 1',
      checkbox: {label: '', checked: false},
    },
    {
      id: uuidv4(),
      avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-367-456319.png',
      title: 'Ant Design Title 2',
      detail: 'Ant Design, a design language for background applications, is refined by Ant UED Team 2',
      checkbox: {label: '', checked: false},
    },
    {
      id: uuidv4(),
      avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-372-456324.png',
      title: 'Ant Design Title 3',
      detail: 'Ant Design, a design language for background applications, is refined by Ant UED Team 3',
      checkbox: {label: '', checked: false},
    },
    {
      id: uuidv4(),
      avatar: 'https://cdn.iconscout.com/icon/free/png-256/avatar-380-456332.png',
      title: 'Ant Design Title 4',
      detail: 'Ant Design, a design language for background applications, is refined by Ant UED Team 4',
      checkbox: {label: '', checked: false},
    },
  ],
};

export const listReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_LIST:
      return {...state, list: [...state.list, {...action.value, checkbox: {label: '', checked: false}}]};
    case EDIT_CHECK_LIST:
      return {
        ...state,
        list: state.list.filter(obj => {
          if (obj.id == action.id) {
            obj.checkbox.checked = action.value;
            return obj;
          }
          return obj;
        }),
      };
    case EDIT_LIST:
      return {
        ...state,
        list: state.list.filter(obj => {
          if (obj.id == action.id) {
            obj.title = action.value.title;
            obj.detail = action.value.detail;
            obj.avatar = action.value.avatar;
            return obj;
          }
          return obj;
        }),
      };
    case CHECK_ALL_LIST:
      return {
        ...state,
        list: state.list.filter(obj => {
          obj.checkbox.checked = action.value;
          return obj;
        }),
      };
    case DELETE_LIST:
      return {...state, list: state.list.filter(obj => obj.id !== action.id)};
    case DELETE_ALL_LIST:
      return {...state, list: state.list.filter(obj => obj.checkbox.checked != true)};
    default:
      return state;
  }
};
