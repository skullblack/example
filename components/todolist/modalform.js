import React, {useEffect, memo} from 'react';

import {Modal, Form, Input} from 'antd';

const ModalForm = memo(({visible, onCreate, onCancel, data}) => {
  const [form] = Form.useForm();

  useEffect(() => {
    if (data) {
      form.setFieldsValue(data);
    }
  }, [data]);

  const onCancelModal = () => {
    onCancel();
    form.resetFields();
  };

  return (
    <Modal
      visible={visible}
      title="Create a new collection"
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancelModal}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            onCreate(values, data?.id);
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}>
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        // ไว้ set default field ที่ต้องการ  ชื่อ field : ค่า field
        // initialValues={{
        //   modifier: 'public',
        // }}
      >
        <Form.Item
          name="avatar"
          label="Avatar"
          rules={[
            {
              required: true,
              message: 'Please input the avatar of collection!',
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: 'Please input the title of collection!',
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item name="detail" label="Detail">
          <Input type="textarea" />
        </Form.Item>
      </Form>
    </Modal>
  );
});
export default ModalForm;
