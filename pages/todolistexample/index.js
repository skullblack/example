import React, {useState, useCallback, useMemo} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Checkbox, Avatar, List, Popconfirm, message, Button, Pagination} from 'antd';
import {v4 as uuidv4} from 'uuid';
import ModalForm from '../../components/todolist/modalform';
import {UserOutlined} from '@ant-design/icons';
import 'antd/dist/antd.css';

const TodoList = () => {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState({visible: false, data: ''});
  const [pagination, setPagination] = useState({pageNumber: 1, pageSize: 10});

  const list = useSelector(state => state.listReducer.list);
  const getCheckAll = useSelector(state => state.listReducer.list.filter(obj => obj.checkbox.checked == false).length);

  console.log('Global reRender');
  const onCreate = useCallback(
    (values, id) => {
      if (id) {
        console.log('onEdite: reRender');
        dispatch({type: 'EDIT_LIST', id: id, value: values});
        setVisible({visible: false, data: ''});
      } else {
        console.log('onAdd: reRender');
        dispatch({
          type: 'ADD_LIST',
          value: {id: uuidv4(), avatar: values.avatar, detail: values.detail, title: values.title},
        });
        setVisible({visible: false, data: ''});
      }
    },
    [visible]
  );

  const onEdite = values => {
    console.log('onEditeModal: reRender');
    setVisible({visible: true, data: values});
  };

  const onCancel = useCallback(() => {
    console.log('onCancelModal: reRender');
    setVisible({visible: false, data: ''});
  }, [visible]);

  const onChangePagination = useMemo(() => {
    console.log('onChangePagination: reRender');
    let dataAfterFilter = list.filter(
      (_, i) =>
        i >= pagination.pageNumber * pagination.pageSize - pagination.pageSize &&
        i <= pagination.pageNumber * pagination.pageSize - 1
    );
    return dataAfterFilter;
  }, [pagination, list.length]);

  return (
    <div style={{paddingTop: 40, paddingBottom: 40}}>
      <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '88vh'}}>
        <main style={{width: '70%', minWidth: 700}}>
          {list.length ? (
            <div style={{display: 'flex', justifyContent: 'space-between'}}>
              <Checkbox
                indeterminate={list.length !== getCheckAll ? !!getCheckAll : false}
                onChange={() => dispatch({type: 'CHECK_ALL_LIST', value: !!getCheckAll})}
                checked={!getCheckAll && list.length ? !getCheckAll : false}>
                Checked All
              </Checkbox>
              {list.length - getCheckAll >= 2 ? (
                <Popconfirm
                  title={`Are you sure to delete "${list.length - getCheckAll}" task?`}
                  onConfirm={() => {
                    dispatch({type: 'DELETE_ALL_LIST'});
                    message.success('Click on Yes');
                  }}
                  onCancel={() => message.error('Click on No')}
                  okText="Yes"
                  cancelText="No">
                  <a key="list-loadmore-more" style={{color: '#f5222d'}} href="#">
                    Delete All
                  </a>
                </Popconfirm>
              ) : null}
            </div>
          ) : null}
          <List
            footer={
              <>
                <div style={{marginBottom: 12, textAlign: 'right'}}>
                  <Pagination
                    size="small"
                    onChange={(pageNumber, pageSize) => setPagination({pageNumber, pageSize})}
                    total={list.length}
                  />
                </div>
                <Button type="dashed" onClick={() => setVisible({visible: true})} block>
                  Add field
                </Button>
              </>
            }
            dataSource={onChangePagination}
            renderItem={(item, i) => (
              <List.Item
                actions={[
                  <a key="list-loadmore-edit" onClick={() => onEdite(item)}>
                    Edit
                  </a>,
                  <Popconfirm
                    title="Are you sure to delete this task?"
                    onConfirm={() => {
                      dispatch({type: 'DELETE_LIST', id: item.id});
                      message.success('Click on Yes');
                    }}
                    onCancel={() => message.error('Click on No')}
                    okText="Yes"
                    cancelText="No">
                    <a key="list-loadmore-more" style={{color: '#f5222d'}} href="#">
                      Delete
                    </a>
                    ,
                  </Popconfirm>,
                ]}>
                <div style={{marginRight: 20}}>
                  <Checkbox
                    checked={item.checkbox.checked}
                    onChange={() => dispatch({type: 'EDIT_CHECK_LIST', id: item.id, value: !item.checkbox.checked})}
                  />
                </div>
                <List.Item.Meta
                  avatar={<Avatar src={item.avatar} icon={<UserOutlined />} size="large" />}
                  title={<a href="https://ant.design">{item.title}</a>}
                  description={item.detail}
                />
              </List.Item>
            )}
          />
          <ModalForm visible={visible.visible} data={visible.data} onCreate={onCreate} onCancel={onCancel} />
        </main>
      </div>
    </div>
  );
};

export default TodoList;
