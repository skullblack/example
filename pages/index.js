import React from 'react';
import Link from 'next/link';
import {Typography, Divider, List} from 'antd';
import 'antd/dist/antd.css';

const data = ['To Do List Example with Redux'];

const Home = () => {
  return (
    <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh'}}>
      <main style={{width: '70%', minWidth: 700}}>
        <Divider orientation="left">Example Ant Design</Divider>
        <List
          footer={<div>Work Shop Next JS</div>}
          bordered
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <Typography.Text mark>
                <Link href="/todolistexample">[GO TO DEMO]</Link>
              </Typography.Text>
              {item}
            </List.Item>
          )}
        />
      </main>
    </div>
  );
};

export default Home;
