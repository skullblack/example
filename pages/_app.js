import App from 'next/app';
import {Provider} from 'react-redux';
import React from 'react';
import withRedux from 'next-redux-wrapper';
import store from '../store/store';

const MyApp = ({Component, pageProps}) => {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
};
const makeStore = () => store;
export default withRedux(makeStore)(MyApp);
